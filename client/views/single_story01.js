if (Meteor.isClient) {
	
Template.single_story01.helpers({
	currentStory: function() {
		return Stories.findOne({_id:Session.get('currentStoryId')});
	},

	storyTitle : function() {
		return Stories.findOne({_id:Session.get('currentStoryId')}).title;
	},

	storyDate : function() {
		return Stories.findOne({_id:Session.get('currentStoryId')}).date;
	},

	storyAuthor : function() {
		return Stories.findOne({_id:Session.get('currentStoryId')}).author;
	},

	storyDescription : function() {
		return Stories.findOne({_id:Session.get('currentStoryId')}).story_desc;
	},

	storyText : function() {
		return Stories.findOne({_id:Session.get('currentStoryId')}).story_text;
	}

	// storyDate : function() {
	// 	return currentStory().date
	// },

	// storyAuthor : function() {
	// 	return currentStory().author
	// },

	// storyDescription : function() {
	// 	return currentStory().story_desc
	// },

	// storyText : function() {
	// 	return currentStory().story_text
	// }
	
});
// Template.lot.lot = -> collections.Lots.findOne({_id:Session.get('currentLot')})
}
