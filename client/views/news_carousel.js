if (Meteor.isClient) {

	Template.news_carousel.stories = function(){
		// return Stories.find({}, {sort:{date: -1}})
		return Stories.find({in_news_carousel:true}, {sort:{date: -1}})
		// console.log('The news object is ' + news)
		
	}
	Template.news_carousel.rendered = function(){
		Deps.autorun(function(){
			// Meteor.subscribe("stories",Meteor.userId())
			Meteor.subscribe("stories")
			// Meteor.subscribe("likes")
		});// why do we wrap this in Deps.autorun? // I thought sub scriptions were alreay reactive data sources
	}


}