if (Meteor.isClient){
	Template.main_slider.stories = function(){
		// return Stories.find({}, {sort:{date: -1}})
		return Stories.find({in_main_slider:true}, {sort:{date: -1}})

	}

	Template.main_slider.rendered = function(){
		Deps.autorun(function(){
			// return Stories.find({in_main_slider:true}, {sort:{date: -1}})

			Meteor.subscribe("stories")
			
		});// why do we wrap this in Deps.autorun? // I thought sub scriptions were alreay reactive data sources
	}
}

// Find all the  posts with  main_slider set to true


// when template renders you only want to load the images once. 

// when user clicks on href in template they should be routed to that story. 
