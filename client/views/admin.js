if (Meteor.isClient){
	Template.admin.configs = function(){
		return SiteConfigs.find()
	};
	Template.admin.events({
		'click button.update': function(e){
			console.log('clicked')
			var input = $('#'+this._id)
			var name = input.attr('name')
			var value = input.val()
			SiteConfigs.update(this._id, {$set: {value: value}});
		}
	})
}