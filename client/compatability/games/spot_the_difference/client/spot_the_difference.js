
/**
 * spot-the-difference.js
 *
 * Kids Dailies Spot the Difference JS.
 *
 * @author Bruce Huang <bruce@bmcd.co>
 * @version 0.1
 * @package games
 */
// this should come from a game collection..


if (Meteor.isClient) {
  window.reload;
  var img_data = { "data" : { "rad_array" : [ 16,
          23,
          16,
          20,
          116,
        ],
      "seconds" : 120,
      "x_array" : [ 276,
          264,
          330,
          78,
          122,
        ],
      "y_array" : [ 178,
          124,
          517,
          321,
          533,
        ],
      "img_width": 674,
      "img_height": 708,
    }
};

var total_seconds = img_data.data.seconds + 1;
var x_array = img_data.data.x_array;     
var y_array = img_data.data.y_array;
var rad_array =img_data.data.rad_array;
var img_width = img_data.data.img_width || 500;
var img_height =img_data.data.img_height || 500;
  var x,y;
  var count = 0;
  var length = 0;

// if(document.getElementById("spot_the_difference_wrap") != null)
    // {
  cirkus = function(centerX,centerY, radius ) {
    var spot_canvas=document.getElementById("spot_Canvas");
    var img=document.getElementById('spot_image1'); 

    var obCanvas = spot_canvas.getContext('2d');
   
    obCanvas.beginPath();
    obCanvas.arc(centerX, centerY, radius, 0, 2*Math.PI, false);
    obCanvas.lineWidth = 2;
    obCanvas.strokeStyle = 'blue';
    obCanvas.stroke();
  }

  position = function(e) {
    var x_diff;
    var y_diff;
    var distance0, distance1;
    var width=document.getElementById('spot_Canvas').width;
    var offset_left =  document.getElementById('spot_Canvas').offsetLeft;
    var offset_top =  document.getElementById('spot_Canvas').offsetTop;

    e = arguments.callee.caller.arguments[0] || window.event

    if (e.pageX == null && e.clientX != null ) { 
      var html = document.documentElement
      var body = document.body
  
      e.pageX = e.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0)
      e.pageY = e.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0)
    }
    x=e.pageX;
    y=e.pageY;
    x = x - offset_left;
    y = y - offset_top;

    if(x_array.length == y_array.length) {
      length = x_array.length;
      for (var i = 0; i < length; i++) {
        x_diff=x_array[i]-x;
        y_diff=y_array[i]-y;
        x_diff1=x_diff+width/2;
        distance0=Math.sqrt(x_diff*x_diff+y_diff*y_diff);
        distance1=Math.sqrt(x_diff1*x_diff1+y_diff*y_diff);
        
        if ( (distance0 < rad_array[i])||(distance1 < rad_array[i]))  {
          cirkus(x_array[i],y_array[i],rad_array[i]);
          cirkus(x_array[i]+width/2,y_array[i],rad_array[i]);
          count++;

          if(length == count) {
            clearInterval(spot_timer);
            document.getElementById("spot_timer").innerHTML = 'Congratulations!';
            
            alert('Congratulations! You\'ve found all the ' + length + ' differences');
          }
          
        }
      }
    }
  }

  toHHMMSS = function(seconds) {
    var sec_num = parseInt(seconds); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = hours+':'+minutes+':'+seconds;
    return time;
  }

  spot_img = function() {
    var spot_canvas=document.getElementById("spot_Canvas");
    var spot_img=document.getElementById('spot_image1'); 
    spot_canvas.width = img_width;
    spot_canvas.height =img_height;


    //Refer to the image
    var obCanvas = spot_canvas.getContext('2d');
    obCanvas.drawImage(spot_img,0,0);
    obCanvas.stroke();
  }

  updateTimer = function () {
    if(document.getElementById("spot_timer_number") != null && typeof(spot_timer) != 'undefined')
    {
      total_seconds -= 1;
      document.getElementById("spot_timer_number").innerHTML = toHHMMSS(total_seconds);
      
      if(total_seconds <= 0)
      {
        clearInterval(spot_timer);
        alert('Time Out, click to reload page to restart the game.');
        location.reload();
      } 

    }else if(typeof(spot_timer) != 'undefined')
    {
      total_seconds = 0;
      clearInterval(spot_timer);
    }
    
  }

 // var timer = setInterval(updateTimer, 1000); 

 //  window.onload = function() {
 //    // Set up the canvas.
    
      
 //    if(document.getElementById("spot_timer_number") != null)
 //    {
 //      spot_img();    
 //    }
      
    

 //  };
  
  Template.spot_the_difference.rendered = function () {
    // Set up the canvas.

   
    var spot_timer = setInterval(updateTimer, 1000); 
    setTimeout(spot_img, 1000);
  };

  Template.spot_the_difference.events({
    'click #spot_Canvas': function(evt) {
        position(evt);
    }
  });
}
