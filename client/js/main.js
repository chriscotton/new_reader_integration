// all the jquery bits

$(document).ready(function() {
    event.preventDefault()

    var menuLeft = document.getElementById('cbp-spmenu-s1'),
        showLeftPush = document.getElementById('showLeftPush'),
        body = document.body;


    showLeftPush.onclick = function() {
        classie.toggle(this, 'active');
        classie.toggle(body, 'cbp-spmenu-push-toright');
        classie.toggle(menuLeft, 'cbp-spmenu-open');
        disableOther('showLeftPush');
    };

    function disableOther(button) {

        if (button !== 'showLeftPush') {
            classie.toggle(showLeftPush, 'disabled');
        }
    }





    $('body').attr('cbp-spmenu-push-toright');

    // $('#js-news').ticker({
    //     titleText: '',
    //     controls: false,
    // });
    
    // $('.carousel').carousel();
    
    $('.news_carousel').carousel();

    $('.news_box_carousel').carousel();

    
    $('.main_carousel').carousel();
    
    $('.box_carousel').carousel();
    
    $("a.prettyPhoto").prettyPhoto();

    $(".prettyPhotolink").click(function() {
        var thisgal = $(this).attr('rel');
        $("a.prettyPhoto." + thisgal + ":first").click()
    });
});