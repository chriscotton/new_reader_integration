# '/': 
# 	to: 'main'
# 	and: () -> 
# 			Session.set 'isHome', true
# 			console.log 'home route ' + Session.get 'isHome'
if Meteor.isClient
	Meteor.Router.add
		'/story_list': 
			to: 'story_list'
			and: -> 
					Session.set 'isHome', true
					console.log 'home route ' + Session.get 'isHome'

		'/home': 
			to: 'widget_homepage'
			and: -> 
					Session.set 'isHome', true
					console.log 'home route ' + Session.get 'isHome'

		
		'/fake_game' : 'fake_game'

		'/info_graphic' : 
			to: 'info_graphic'
			and: -> 
					Session.set 'isHome', false
					console.log 'home route ' + Session.get 'isHome'

		'/image_scramble' : 
			to: 'image_scramble'
			and: -> 
					Session.set 'isHome', false
					console.log 'home route ' + Session.get 'isHome'

		'/maze_2' : 
			to: 'maze_2'
			and: -> 
					Session.set 'isHome', false
					console.log 'home route ' + Session.get 'isHome'

		'/maze' : 
			to: 'maze'
			and: () -> 
					Session.set 'isHome', false
					console.log 'home route ' + Session.get 'isHome'

		'/spot_the_difference' : 
			to: 'spot_the_difference'
			and: -> 
					Session.set 'isHome', false
					console.log 'home route ' + Session.get 'isHome'

		'/wordsearch' : 
			to: 'wordsearch'
			and: () -> 
					Session.set 'isHome', false
					console.log 'home route ' + Session.get 'isHome'

		'/crossword' : 
			to: 'crossword'
			and: () -> 
					Session.set 'isHome', false
					console.log 'home route ' + Session.get 'isHome'

		'/tic-tac-toe' : 
			to: 'tic-tac-toe'
			and: () -> 
					Session.set 'isHome', false
					console.log 'home route ' + Session.get 'isHome'

		'/stories/:_id' : 
			to: 'single_story01' # remember this doubles as the name of the route
			and: (_id) ->
					# Session.set 'currentPage', this.id
					Session.set 'currentStoryId', _id					
					Session.set 'isHome', false
					console.log 'our story id is ' + Session.get 'currentStoryId'
					console.log 'home route ' + Session.get 'isHome'
					console.log 'we are at ' + this.canonicalPath 
		'/admin' :
			to: 'admin'			
		
		
		# '/games/fake_game/fake_game.html' : 'fake_game' # why does 
		# '/story' : 'single_story01'
		# '/' : 
		# 	to: 'widget_homepage'
		# 	and: (id) -> 
		# 			Session.set 'isHome', true
		# 			console.log ('home route ' + isHome)
