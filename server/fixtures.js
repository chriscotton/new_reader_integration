// text generated with http://baconipsum.com/?paras=5&type=meat-and-filler&start-with-lorem=1

if (Stories.find().count() === 0) {
  Stories.insert({
    title: 'Great Story 01',
    author: 'admin01',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: true,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 02',
    author: 'admin02',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: true,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 03',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: true,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });
    Stories.insert({
    title: 'Great Story 04',
    author: 'admin01',
    main_photo: '/img/12.jpg',
    thumb: '/img/12.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id', 
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 05',
    author: 'admin02',
    main_photo: '/img/6.jpg',
    thumb: '/img/6.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 06',
    author: 'admin03',
    main_photo: '/img/1.jpg',
    thumb: '/img/1.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

    Stories.insert({
    title: 'Great Story 07',
    author: 'admin01',
    main_photo: '/img/15.jpg',
    thumb: '/img/15.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 08',
    author: 'admin02',
    main_photo: '/img/2.jpg',
    thumb: '/img/2.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 09',
    author: 'admin03',
    main_photo: '/img/3.jpg',
    thumb: '/img/3.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });
    Stories.insert({
    title: 'Great Story 10',
    author: 'admin01',
    main_photo: '/img/14.jpg',
    thumb: '/img/14.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 11',
    author: 'admin02',
    main_photo: '/img/7.jpg',
    thumb: '/img/7.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: true,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 12',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });
    Stories.insert({
    title: 'Great Story 13',
    author: 'admin01',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 14',
    author: 'admin02',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 15',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });
    Stories.insert({
    title: 'Great Story 16',
    author: 'admin01',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 17',
    author: 'admin02',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 18',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 19',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 20',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'

  });

  Stories.insert({
    title: 'Great Story 21',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });

  Stories.insert({
    title: 'Great Story 22',
    author: 'admin03',
    main_photo: '/img/39.jpg',
    thumb: '/img/39.jpg', 
    tags: ['business', 'politics','sports', 'technology'],
    url: '/stories/:_id',
    story_desc: 'A very short description for this awesome Story!!!',
    in_main_slider: false,
    in_news_carousel: false,
    date:new Date(),
    story_text:'This is the meat of the story. Thank you bacon ipsum!! Short ribs tenderloin doner dolore swine, tri-tip pork belly in labore ham brisket cupidatat fatback ham hock velit. Voluptate ut dolore chicken kielbasa. Exercitation commodo drumstick incididunt qui, corned beef excepteur. Biltong aute nisi chicken, pork chop spare ribs ut strip steak id jowl pork belly venison. Eiusmod chicken nisi capicola. Shoulder veniam sunt brisket tempor tri-tip pork belly fugiat beef spare ribs mollit incididunt chuck do. Prosciutto shank chicken cillum anim turkey ut venison exercitation sint nulla.'
  });


}

if (Issues.find().count() === 0) {
  Issues.insert({
    number: '01',
    title: 'Test Issue 01',
    author: 'Sacha Greif',
    stories: ['story01_id', 'story02_id', 'story03_id', 'story04_id', 'story05_id', 'story06_id', 'story07_id'],
    url: '/issues/:_id', 
    games: ['wordsearch']
  });

  Issues.insert({
    number: '02',
    title: 'Test Issue 02',
    author: 'Tom Coleman ',
    stories: ['story08_id', 'story09_id', 'story11_id', 'story12_id', 'story13_id', 'story14_id', 'story15_id'],
    url: '/issues/:_id', 
    games: ['wordsearch']
  });

  Issues.insert({
    number: '03',
    title: 'Test Issue 03',
    author: 'Tom Coleman',
    stories: ['story16_id', 'story17_id', 'story18_id', 'story19_id', 'story20_id', 'story21_id', 'story22_id'],
    url: '/issues/:_id', 
    games: ['wordsearch']
  });
}

if (SiteConfigs.find().count() === 0) {
    SiteConfigs.insert({
        name: 'header_banner',
        value: '/img/Daily10_logo.png'
    })
}